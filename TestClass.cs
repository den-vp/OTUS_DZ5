﻿using Otus_DZ5.Attributes;

namespace ReflectionDZ5
{   [LastChange("Denis", "15-06-2021", 123456)]
    public class TestClass
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int i3 { get; set; }
        public int i4 { get; set; }
        public int i5 { get; set; }
        [LastChange("Method GetTestClass", "16-06-2021", 111222)]
        public static TestClass GetTestClass() => new TestClass() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }
}