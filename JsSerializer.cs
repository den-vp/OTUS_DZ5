﻿using System.IO;
using System.Text.Json;

namespace ReflectionDZ5
{
    public class JsSerializer : ISerializer
    {
        public void Serialize<TmyObj>(TmyObj obj, FileStream file)
        {
            string jsontext = JsonSerializer.Serialize<TmyObj>(obj);
            byte[] array = System.Text.Encoding.Default.GetBytes(jsontext);
            file.Write(array, 0, array.Length);

            /*using (StreamWriter writer = new StreamWriter(file))
            using (JsonTextWriter jsonWriter = new JsonTextWriter(writer))
            {
                JsonSerializer ser = new JsonSerializer();
                ser.Serialize(jsonWriter, obj);
                jsonWriter.Flush();
            }*/
        }
        public TmyObj Deserialize<TmyObj>(FileStream data) where TmyObj : new()
        {
            byte[] array = new byte[data.Length];
            data.Read(array, 0, array.Length);
            string strData = System.Text.Encoding.Default.GetString(array);
            return JsonSerializer.Deserialize<TmyObj>(strData);

            /*var serializer = new JsonSerializer();
            using (var sr = new StreamReader(data))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return (MyObj)serializer.Deserialize(jsonTextReader);
            }*/
        }
    }
}