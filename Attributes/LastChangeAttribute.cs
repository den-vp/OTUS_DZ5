﻿using System;

namespace Otus_DZ5.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    class LastChangeAttribute : System.Attribute
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private readonly string date;
        public string Date
        {
            get { return date; }
        }
        private int numTask;
        public int NumTask
        {
            get { return numTask; }
            set { numTask = value; }
        }
        public LastChangeAttribute(string name, string date, int numTask)
        {
            this.name = name;
            this.date = date;
            this.numTask = numTask;
        }        

    }
}