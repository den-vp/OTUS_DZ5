﻿using System.IO;

namespace ReflectionDZ5
{
    interface ISerializer
    {
        void Serialize<TmyObj>(TmyObj obj, FileStream file);

        TmyObj Deserialize<TmyObj>(FileStream data) where TmyObj : new();
    }
}