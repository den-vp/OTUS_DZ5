﻿using Otus_DZ5.Attributes;
using System;
using System.Linq;
using System.Reflection;

namespace ReflectionDZ5
{
    class Program
    {
        static void Main(string[] args)
        {
            TestClass test = TestClass.GetTestClass();
            Type type = typeof(TestClass);
            object[] attributes = type.GetCustomAttributes(false);
            foreach (LastChangeAttribute attr in attributes)
            {
                Console.WriteLine($"Анализ типа : Name - {attr.Name}; Date - {attr.Date}; NumTask - {attr.NumTask}");
            }

            MethodInfo method = type.GetMethod("GetTestClass",
                BindingFlags.Public | BindingFlags.Static);
            attributes = method.GetCustomAttributes(typeof(LastChangeAttribute), false);

            foreach (LastChangeAttribute attr in attributes)
            {
                Console.WriteLine($"Анализ метода: Name - {attr.Name}; Date - {attr.Date}; NumTask - {attr.NumTask}");
            }

            Console.WriteLine($"{(type.GetCustomAttribute(typeof(LastChangeAttribute)) as LastChangeAttribute).Name}; {(type.GetCustomAttribute(typeof(LastChangeAttribute)) as LastChangeAttribute).Date}; {(type.GetCustomAttribute(typeof(LastChangeAttribute)) as LastChangeAttribute).NumTask}");

            Assembly assembly = Assembly.GetExecutingAssembly();
            object[] attr1 = assembly.GetCustomAttributes(false);

            foreach (Attribute attribute in attr1)
            {
                Console.WriteLine("Attribute: {0}", attribute.GetType().Name);
            }


            int iterationCount = 100_000;
            TestSerializer testSerializer = new TestSerializer(iterationCount, new CsvSerializer(), ".csv");
            Console.WriteLine(testSerializer.TestRunSerializing(test));

            testSerializer = new TestSerializer(iterationCount, new JsSerializer(), ".json");
            Console.WriteLine(testSerializer.TestRunSerializing(test));

            testSerializer = new TestSerializer(iterationCount, new XmlSerial(), ".xml");
            Console.WriteLine(testSerializer.TestRunSerializing(test));

            Console.ReadKey();
        }
    }
}